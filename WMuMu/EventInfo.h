#ifndef TTM_EventInfo_H
#define TTM_EventInfo_H

class WMuMuTupleMaker ;

#include "Common.h"

namespace WMuMu
{
  class EventInfo
  {
  public: 
    friend class ::WMuMuTupleMaker ;
    EventInfo() : bxid(0),eventnumber(0),runnumber(0) {}
    //virtual ~EventInfo() {}
    //template<class LHCbEventInfo>
    //EventInfo(const LHCbEventInfo& p) :
  private:
    // some event if info
    unsigned short    bxid ;
    unsigned long int eventnumber ;
    unsigned int      runnumber ;
    // add recsummary info?
    //ClassDef(EventInfo,1)
  } ;
}

#endif
