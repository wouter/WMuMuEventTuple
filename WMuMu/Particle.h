#ifndef TTM_Particle_H
#define TTM_Particle_H

class WMuMuTupleMaker ;

#include "MomentumVector.h"
#include "Common.h"
#include "TObject.h"
#include "DecisionInfo.h"

namespace WMuMu
{

  class Particle : public MomentumVector
  {
  public: 
    friend class ::WMuMuTupleMaker ;
    Particle() {}
    virtual ~Particle() ;
    template<class LHCbParticle>
      Particle(const LHCbParticle& p) :
    MomentumVector(p.momentum()),
	pid(p.particleID().pid()) {}

    //TLorentzVector p4() const { return mom.p4() ; }
    //operator TLorentzVector() const {
    //  return p4() ;
    //}
    //return TLorentzVector(MomentumVector::Px,MomentumVector::Py,MomentumVector::Pz,MomentumVector::E()) ;
    //}

  public:
    //MomentumVector mom ;
    int pid ;
    //ClassDef(Particle,1)
    ParticleTisTos decinfo ;
  } ;
}

#endif
