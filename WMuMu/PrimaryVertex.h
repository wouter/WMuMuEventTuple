#ifndef WMUMU_PRIMARYVERTEX
#define WMUMU_PRIMARYVERTEX

namespace WMuMu
{
  class PrimaryVertex
  {
  public:
  PrimaryVertex() : x(0),y(0),z(0),ntrack(0),nbackward(0),nlong(0),sumpt(0) {}
  public:
    float x ;
    float y ;
    float z ;
    unsigned char ntrack ;
    unsigned char nbackward ;
    unsigned char nlong ;
    float sumpt ;
  } ;
}

#endif
