#ifndef WMuMuXContainer_H
#define WMuMuXContainer_H

#include "WToMuMuXCandidate.h"


namespace WMuMu
{
  class WToMuMuXContainer
  {
 public:
  WToMuMuXContainer() {}
  const WToMuMuXCandidate* bestSingleJetCandidate() const ;
  const WToMuMuXCandidate* bestDiJetCandidate() const ;
  size_t numSingleCandidate() const ;
  void clear() ;
 public:
  std::vector<WToMuMuXCandidate> candidates ;
} ;
  
}

#endif
