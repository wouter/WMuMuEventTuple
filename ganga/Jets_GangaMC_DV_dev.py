Application = DaVinci()
Application.version           = "v41r2"
Application.user_release_area = "$PWD/../../DaVinciDev_v41r2"
Application.optsfile          = [File("$PWD/../options/JetTuple_MC.py")]
j = Job(name = 'Jets_Ganga_MC_TEST',
        application    = Application,
        backend        = Dirac(),
  	    postprocessors = RootMerger(files = ["JetTuple.root"], overwrite=True, ignorefailed=True),
        splitter       = SplitByFiles(filesPerJob   = 10 ,  
                                      ignoremissing = False ) ,
        outputfiles    = [LocalFile(namePattern = "*.root")] ,
        inputdata      = [LocalFile("/eos/lhcb/user/c/cvazquez/piV_dst/00042400_00000002_2.AllStreams.dst")])
j.submit()

