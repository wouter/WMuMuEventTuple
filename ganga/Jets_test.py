mydir = '/afs/cern.ch/user/i/ikostiuk/cmtuser/'

j=Job(
    application = GaudiExec( 
    directory =    mydir + "DaVinciDev_v41r2" , 
    options   = [  mydir + "DaVinciDev_v41r2/HV_piVpiV_Run2/WMuMuEventTuple/options/JetTuple_new.py" ] ,
    ) ,
    backend      = Dirac () ,
    splitter     = SplitByFiles ( filesPerJob   =     10 ,  
                                  ignoremissing = False ) ,
    outputfiles  = [ LocalFile ( '*.root' ) ] ,
    )

j.name = 'Jets_test'
j.comment = 'Jets test with Wouters files with Stripping28/90000000/BHADRONCOMPLETEEVENT.DST'

## get input data from DB 
query = BKQuery('/LHCb/Collision16/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco16/Stripping28/90000000/BHADRONCOMPLETEEVENT.DST')

data  = query.getDataset()
## take only first 250 files.
data  = data[:250]
j.inputdata = data

j.submit()

