from Configurables import EventSelector

import glob
files = []
files += glob.glob('/data/bfys/wouterh/Z0/2016/StrippingZ02MuMu.MD.*.dst')
input =  [ 'PFN:%s' % i for i in files]

from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles( input, clear = True )
print input

from Configurables import DaVinci
DaVinci().DataType = "2016"
DaVinci().Simulation = False
DaVinci().PrintFreq = 100

#IOHelper('ROOT').inputFiles([
#    'PFN:/data/bfys/wouterh/Z02015/zmumu.md.all.dst',
#    'PFN:/data/bfys/wouterh/Z02015/zmumu.mu.all.dst'
#    ], clear=True )
#from Configurables import IODataManager
#IODataManager().AgeLimit = 2
