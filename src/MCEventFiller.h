#ifndef MCEVENTFILLER_H
#define MCEVENTFILLER_H

void fillMCVirtualJetInfo(const LHCb::MCParticles& mcparticles,
			  WMuMu::MCEventInfo& mcevent) ;

#endif
